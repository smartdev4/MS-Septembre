# masterHtml
a Gulp project for HTML working by Lynh Nguyen

**Supported:**
- Nodejs v12+
- Gulp 3.9.1
- _js:_ concat, hint, minify, convert es6 to es5
- _css:_ sass, concat, minify, auto prefixer to work in all browsers
- _html:_ include file, pass parameters... like php project

**PreInstall Packages:**
- _jquery_
- _bootstrap_
- _fontawesome_
- _fancybox_
- _moment_
- _wow_
- _swiper_

**Structure:**
- _src:_ workspace - place to work
- _dist:_ distribution - place to run

**Using:**
- Clone this
- run  npm install
- open command line then go to masterHtml by using _cd_ command
- run  npm start
- That's it!

